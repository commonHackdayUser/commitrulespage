import React from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jiraNumber: "",
      jiraDescription: "",
      isLinkJira: false
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData(event.target);
    console.log(JSON.stringify({
      jira: this.state.jiraNumber,
      comment: this.state.jiraDescription, 
      isJiraLink: this.state.isLinkJira,
    }));

    fetch('http://helmesbel.by:8095/rulesApi/allChanged', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        jira: this.state.jiraNumber,
        comment: this.state.jiraDescription, 
        isJiraLink: this.state.isLinkJira,
      })
    }).then(function(response) {
      if (response.ok) {
        console.log("Ok");
        return response
      } else {
        var error = new Error(response.statusText)
        error.response = response
        throw error
      }
    }).catch(function() {
       console.log("error");
  });  
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="jiraForm">
          <div css="float:left;margin-right:20px;">
          <label htmlFor="jiraNumber" className="labelOnTheTop">Jira</label>        
          <input name="jiraNumber" type="text" id="jiraNumber" className="inutFields" value={this.state.value} onChange={this.handleChange}/>
          </div>
          <div>
            <label htmlFor="jiraDescription" className="labelOnTheTop">Description</label>   
            <textarea name="jiraDescription" type="text" id="jiraDescription" className="inutFields" value={this.state.value} onChange={this.handleChange}/>   
            <span className="smallText">total changes: </span>
            <span className="smallText">deleted: </span>
            <span className="smallText">updated: </span>
            <span className="smallText">new: </span>     
          </div>         
          <div>
            <label htmlFor="linkJira" className="labelOnTheLeft">Link to the jira</label>
            <input name="isLinkJira" className="chkbox" type="checkbox" id="linkJira" value={this.state.value} onChange={this.handleChange}></input>
          </div> 
          <div>
            <input className="submit" type="submit" value="Save"/>
          </div>      
        </div>     
      </form>    
    );
  }
}

export default App;
